import { JSDOM } from "jsdom"
const dom = new JSDOM()
document = dom.window.document

const { Stint } = require("./index")
const { Status } = require("./index")

describe('Check Stint class, set start and end times', (): void => {
    const stint = new Stint(new Date(2020, 1, 2, 3, 4, 5, 678))
    stint.finishStint(new Date(2020, 1, 2, 3, 45, 6, 789))

    test('Construcct Stint instance', (): void => {
        const response: Date = stint.startTime;
        expect(response).toStrictEqual(new Date(2020, 1, 2, 3, 4, 5, 678))
    })

    test('Finish Stint', (): void => {
        const response: Date = stint.endTime;
        expect(response).toStrictEqual(new Date(2020, 1, 2, 3, 45, 6, 789))
    })

    test('Convert date to string HH:MM, not padding', (): void => {
        const date = new Date(2020, 1, 2, 13, 14, 5, 678)

        const response: string = stint.convertTimeToHHcMM(date)
        expect(response).toStrictEqual("13:14")
    })

    test('Convert date to string HH:MM, 0 padding', (): void => {
        const date = new Date(2020, 1, 2, 3, 4, 5, 678)

        const response: string = stint.convertTimeToHHcMM(date)
        expect(response).toStrictEqual("03:04")
    })

    test('Convert undefined to string HH:MM', (): void => {
        const response: string = stint.convertTimeToHHcMM()
        expect(response).toStrictEqual("--")
    })

    test('Calc stint duration', (): void => {
        const response: number = stint.returnDuration()
        expect(response).toBe(2461)
    })

    test('Return stint duration string', (): void => {
        const response: string = stint.returnDurationString()
        expect(response).toBe("41m 1s")
    })

    test('Return times string', (): void => {
        const response: string = stint.returnTimes()
        expect(response).toBe("03:04 -- 03:45")
    })
})

describe('Check Stint class, Unset end time', (): void => {
    const stint = new Stint(new Date(2020, 1, 2, 3, 4, 5, 678))
    test('Calc stint duration', (): void => {
        const response: number = stint.returnDuration()
        expect(response).toBe(0)
    })

    test('Return stint duration string', (): void => {
        const response: string = stint.returnDurationString()
        expect(response).toBe("")
    })

    test('Return times string', (): void => {
        const response: string = stint.returnTimes()
        expect(response).toBe("")
    })
})

describe('Check Status class', (): void => {
    const status = new Status()

    test('Start new stint', (): void => {
        status.startNewStint()

        const response = status.stint
        expect(response.length).toBe(1)
        expect(response[0].startTime).toBeDefined()
    })

    test('Finish current stint', (): void => {
        const pastNo: number = status.stintNo
        status.finishCurrentStint()

        const response = status.stint[status.stint.length - 1]
        expect(response.endTime).toBeDefined()
        expect(status.stintNo - pastNo).toBe(1)
    })

    test('Calc diff, Unixtime', (): void => {
        const response: number = status.calcDiff(1580582706789, 1580580245678)
        expect(response).toBe(2461)
    })
})

describe('Check Status class, start two stints', (): void => {
    const status = new Status()
    status.stint.push(new Stint(new Date(2020, 0, 1, 2, 0, 0)))
    status.stint[0].finishStint(new Date(2020, 0, 1, 2, 0, 30))
    status.stint.push(new Stint(new Date(2020, 0, 1, 2, 1, 0)))
    status.stint[1].finishStint(new Date(2020, 0, 1, 2, 1, 30))

    test('Start new stints', (): void => {
        const response: number = status.stint.length
        expect(response).toBe(2)
    })

    test('Calc break duration', (): void => {
        let response: number = status.returnBreakDuration(3, 3)
        expect(response).toBe(0)

        response = status.returnBreakDuration(4, 3)
        expect(response).toBe(0)

        response = status.returnBreakDuration(-1, 0)
        expect(response).toBe(0)

        response = status.returnBreakDuration(0, 2)
        expect(response).toBe(0)

        response = status.returnBreakDuration(0, 1)
        expect(response).toBe(30)
    })

    test('Calc current duration', (): void => {
        const now: number = (new Date()).getTime()
        const diff = status.calcDiff(now, (new Date(2020, 0, 1, 2, 1, 0)).getTime())

        const response: number = status.returnCurrentDuration()
        expect(response).toBe(diff)
    })

    test('Calc current break duration', (): void => {
        const now: number = (new Date()).getTime()
        const diff = status.calcDiff(now, (new Date(2020, 0, 1, 2, 1, 30)).getTime())

        const response: number = status.returnCurrentBreakDuration()
        expect(response).toBe(diff)
    })
})
