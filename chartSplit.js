var getDocumentsOn = function(day)
{
    const start = (new Date((new Date(day)).toDateString())).getTime()
    const end = start + (24 * 60 * 60 * 1000)

    return new Promise((resolve, reject) =>
                       {
                           firebase.auth().onAuthStateChanged((user) =>
                                                              {
                                                                  if (user)
                                                                  {
                                                                      console.log("user authenticated")

                                                                      const db = firebase.firestore()
                                                                      db.collection("users").doc(user.uid).collection("stints")
                                                                          .where("start", ">=", new firebase.firestore.Timestamp.fromMillis(start))
                                                                          .where("start", "<=", new firebase.firestore.Timestamp.fromMillis(end))
                                                                          .get()
                                                                          .then((querySnapshot) => {
                                                                              console.log("got documents")
                                                                              return resolve(querySnapshot)
                                                                          })
                                                                          .catch((error) => {return reject(error)})
                                                                  }
                                                                  else
                                                                  {
                                                                      return reject()
                                                                  }
                                                              })
                       })
}

var drawGraph = function(barChartData)
{
    chart.data = barChartData
    chart.update()
}

const getDocumentsAndDrawGraph = function(date)
{
    getDocumentsOn(date).then((result) =>
                              {
                                  console.log("show got documents")
                                  console.log(result)

                                  const startTimeArray = result.docs.map(
                                      (doc) =>
                                          {
                                              const milliseconds = (doc.data().start.seconds * 1000) + (doc.data().start.nanoseconds / 1000)
                                              return new Date(milliseconds)
                                          })
                                  const stintMinutes = result.docs.map(doc => (doc.data().finish.seconds - doc.data().start.seconds) / 60)
                                  const breakMinutes = result.docs.map(doc => 5)

                                  const barChartData = {
	                              datasets: [
                                          {
	                                      backgroundColor: "#F3B9DB",
	                                      data: stintMinutes,
	                                  },
                                          {
	                                      backgroundColor: "#B5FFAA",
                                              data: breakMinutes,
	                                  },
                                      ],
                                      labels: startTimeArray,
                                  }

                                  drawGraph(barChartData)
                              })
        .catch(error =>
               {
                   console.error(error)
               })
}

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

const chartConfig =
      {
          options:
          {
              scales:
              {
                  xAxes: [
                      {
                          stacked: true,
                          time:
                          {
                              unit: 'hour',
                          },
                          type: 'time',
                      }
                  ],
                  yAxes: [
                      {
                          stacked: true,
                      },
                  ],
              },
          },
          type: 'bar',
      }

var context
var chart

document.addEventListener("DOMContentLoaded",
                          () =>
                          {
                              context = document.getElementById('myChart').getContext('2d')
                              chart = new Chart(context, chartConfig)

                            document.getElementById('datePicker')
                                .addEventListener('change',(e) =>
                                                  {
                                                      console.log("test")
                                                      const pickedDate = e.srcElement.valueAsNumber
                                                      getDocumentsAndDrawGraph(pickedDate)
                                                  })

                            const datePickerElement = document.getElementById('datePicker')
                            datePickerElement.value = new Date().toDateInputValue();
                            getDocumentsAndDrawGraph(datePickerElement.valueAsNumber)
window.addEventListener("orientationchange",
                        () =>
                        {
                            let canvas = document.getElementById("allContainer")
                            if (screen.orientation.type == "portrait-primary")
                            {
                                canvas.style.height = "30vh"
                                canvas.style.width = "100vw"
                            }
                            else
                            {
                                canvas.style.height = "100vh"
                                canvas.style.width = "100vw"
                            }
                        }, true);
                          })
