declare let firebase: any

let authenticatedUser = null

export class Stint
{
    startTime: Date
    endTime: Date | null

    constructor(start: Date)
    {
        this.startTime = start
        this.endTime = null
    }

    finishStint(endTime: Date): void
    {
        this.endTime = endTime
        recordStint(authenticatedUser, this.startTime, this.endTime)
    }

    convertTimeToHHcMM(time: Date): string
    {
        if (time === undefined)
            return "--"

        const hour: number = time.getHours()
        const min: number = time.getMinutes()

        return ("000" + hour).slice(-2) + ":" + ("000" + min).slice(-2)
    }

    returnDuration(): number
    {
        if (this.endTime === null)
            return 0

        const miliSec: number = (this.endTime.getTime() - this.startTime.getTime()) % 1000
        const sec: number = (this.endTime.getTime() - this.startTime.getTime() - miliSec) / 1000
        return sec
    }

    returnDurationString(): string
    {
        if (this.endTime === null)
            return ""

        const duration = this.returnDuration()

        const sec = duration % 60
        const min = (duration - sec) / 60

        return "" + min + "m " + sec + "s"
    }

    returnTimes(): string
    {
        if (this.endTime === null)
            return ""

        const start = this.convertTimeToHHcMM(this.startTime)
        const end = this.convertTimeToHHcMM(this.endTime)

        return "" + start + " -- " + end
    }
}

export class Status
{
    stintNo: number
    stint: Stint[]

    constructor()
    {
        this.stintNo = 1
        this.stint = []
    }

    startNewStint(): void
    {
        this.stint.push(new Stint(new Date()))
    }

    finishCurrentStint(): void
    {
        const currentStint: Stint | null = this.stint[this.stint.length - 1]

        if (currentStint === null)
            return

        currentStint.finishStint(new Date())
        this.stintNo = this.stintNo + 1
    }

    calcDiff(nextTime: number, pastTime: number): number
    {
        const duration: number = nextTime - pastTime
        const sec: number = Math.floor(duration / 1000)

        return sec
    }

    returnBreakDuration(pastIndex: number, nextIndex: number): number
    {
        if (pastIndex < 0 || pastIndex >= nextIndex || nextIndex >= this.stint.length)
            return 0

        const startTime: Date = this.stint[nextIndex].startTime
        const endTime: Date | null = this.stint[pastIndex].endTime

        if (endTime === null)
            return 0

        const sec = this.calcDiff(startTime.getTime(), endTime.getTime())
        return sec
    }

    returnCurrentDuration(): number
    {
        const latestStint: Stint = this.stint[this.stint.length - 1]
        const sec: number = this.calcDiff((new Date()).getTime(), latestStint.startTime.getTime())

        return sec
    }

    returnCurrentBreakDuration(): number
    {
        const latestStint: Stint = this.stint[this.stint.length - 1]
        if (latestStint.endTime === null)
            return 0

        const sec: number = this.calcDiff((new Date()).getTime(), latestStint.endTime.getTime())

        return sec
    }
}

const pomodoroStatus = new Status()
let stintCounter: any | null = null
let breakCounter: any | null = null

function load()
{
    show()
}


function toggleButton(): void
{
    const buttonNode: Node | null = document.getElementById("button")
    if (buttonNode === null)
        return

    if (buttonNode.textContent === "Start")
    {
        pomodoroStatus.startNewStint()

        if (breakCounter !== null)
            clearInterval(breakCounter)

        renewCounter("stint")
        stintCounter = setInterval(renewCounter, 1000, "stint")
    }
    else
    {
        pomodoroStatus.finishCurrentStint()

        clearInterval(stintCounter)
        renewCounter("break")
        breakCounter = setInterval(renewCounter, 1000, "break")
    }

    show()
}

function setButtonString()
{
    let buttonString = ""
    if (pomodoroStatus.stint.length === 0)
        buttonString = "Start"
    else if (pomodoroStatus.stint[pomodoroStatus.stint.length - 1].endTime === null)
        buttonString = "Stop"
    else
        buttonString = "Start"

    const buttonNode: Node | null = document.getElementById("button")
    if (buttonNode !== null)
        buttonNode.textContent = buttonString
}

function clearHistoryView(history: Node)
{
    while(history.lastChild)
        history.removeChild(history.lastChild)
}

function makeStintHeader(stintNo: number): Node
{
    const element = document.createElement("td")
    element.textContent = "Stint " + stintNo

    return element
}

function makeDurationCell(stint: Stint): Node
{
    const durationCell = document.createElement("td")
    let duration

    if (stint.endTime === null)
        duration = 0
    else
        duration = stint.returnDuration()

    durationCell.textContent = convertDurationToMMmSSs(duration)

    return durationCell
}

function makeTimesCell(stint: Stint): Node
{
    const timesCell = document.createElement("td")
    timesCell.textContent = stint.returnTimes()

    return timesCell
}

function makeStintRow(stintNo: number, stint: Stint): Node
{
    const stintHeader: Node = makeStintHeader(stintNo)
    const timesCell: Node = makeTimesCell(stint)
    const durationCell: Node = makeDurationCell(stint)

    const stintRow = document.createElement("tr")
    stintRow.appendChild(stintHeader)
    stintRow.appendChild(timesCell)
    stintRow.appendChild(durationCell)

    return stintRow
}

function makeBreakHeader(stintNo: number): Node
{
    const breakHeader = document.createElement("td")
    breakHeader.textContent = stintNo % 4 === 0 ? "Big Break" : "Break"

    return breakHeader
}

function makeBreakDurationCell(stintNo: number): Node
{
    const breakDurationCell = document.createElement("td")

    const duration = pomodoroStatus.returnBreakDuration(stintNo - 1, stintNo)
    breakDurationCell.textContent = convertDurationToMMmSSs(duration)

    return breakDurationCell
}

function makeBreakRow(stintNo: number): Node
{
    const breakHeader = makeBreakHeader(stintNo)
    const blanktd = document.createElement("td")
    const breakDurationCell = makeBreakDurationCell(stintNo)

    const breakRow = document.createElement("tr")
    breakRow.appendChild(breakHeader)
    breakRow.appendChild(blanktd)
    breakRow.appendChild(breakDurationCell)

    return breakRow
}

function makeHistory(stints: Stint[]): Node[]
{
    const historyRows: Node[] = []
    stints.forEach((stint, index, array) => {
        const stintRow = makeStintRow(index + 1, stint)
        historyRows.push(stintRow)

        if (stint.endTime === null)
            return

        if (array.length - 1 === index)
            return

        const breakRow = makeBreakRow(index + 1)
        historyRows.push(breakRow)
    })
    return historyRows
}

function show()
{
    const statusNode: Node | null = document.getElementById("status")
    if (statusNode !== null)
        statusNode.textContent = "Stint " + pomodoroStatus.stintNo

    setButtonString()

    const historyNodeNullable: Node | null = document.getElementById("history")
    if (historyNodeNullable === null)
        return

    const historyNode: Node = historyNodeNullable
    clearHistoryView(historyNode)
    
    const historyRows: Node[] = makeHistory(pomodoroStatus.stint)
    historyRows.forEach(row => {historyNode.appendChild(row)})
}

function zeroPadding(num: number, length: number): string
{
    return (Array(length).join("0") + num).slice(-length)
}

function convertDurationToMMcSS(duration: number): string
{
    const sec: number = duration % 60
    const min: number = (duration - sec) / 60

    return zeroPadding(min, 2) + ":" + zeroPadding(sec, 2)
}

function convertDurationToMMmSSs(duration: number)
{
    const sec: number = duration % 60
    const min: number = (duration - sec) / 60

    return zeroPadding(min, 2) + "m " + zeroPadding(sec, 2) + "s"
}

function renewCounter(phase: string)
{
    let countElement
    let duration

    if (phase === "stint")
    {
        countElement = document.getElementById("stintCounter")
        duration = pomodoroStatus.returnCurrentDuration()
    }
    else
    {
        countElement = document.getElementById("breakCounter")
        duration = pomodoroStatus.returnCurrentBreakDuration()
    }

    if (countElement === null)
        return

    countElement.textContent = convertDurationToMMcSS(duration)
}

function recordStint(authenticatedUser: any, starttime: Date, finishtime: Date): void
{
    if (authenticatedUser == null)
    {
        console.warn("it seems like user not authenticated. times were not recorded.")
        return
    }

    const db = firebase.firestore()
    db.collection("users").doc(authenticatedUser.uid).collection("stints").add(
        {
            finish: finishtime,
            start: starttime,
        })
        .then(function() {
            console.log("Document successfully written!")
        })
        .catch(function(error) {
            console.error("Error writing document: ", error)
        })
}

document.addEventListener("DOMContentLoaded", () => {
    load()

    const buttonContainer: Node | null = document.getElementById("buttonContainer")
    if (buttonContainer !== null)
        buttonContainer.addEventListener("click", toggleButton)

    firebase.analytics()

    const firebaseUI = function()
    {
        console.log("sign in ui start")
        window.location.assign("/auth.html")
    }

    const signOut = function()
    {
        console.log("try sign out")
        firebase.auth().signOut()
        console.log("done sign out")
    }

    firebase.auth().onAuthStateChanged(function(user) {
        const signButtonElement = document.getElementById("signButton")
        if (user) {
            // User is signed in.
            // ...
            authenticatedUser = user

            const db = firebase.firestore()
            db.collection("users").doc(authenticatedUser.uid).set({}, {merge: true})
                .then(function(docRef) {
                    console.log("ritten with ID: ", docRef)
                })
                .catch(function(error) {
                    console.error("Error adding document: ", error)
                })

            signButtonElement.innerText = "Sign out"
            signButtonElement.removeEventListener("click", firebaseUI)
            signButtonElement.addEventListener("click", signOut)
        } else {
            // User is signed out.
            // ...
            authenticatedUser = null

            signButtonElement.innerText = "Sign in"
            signButtonElement.removeEventListener("click", signOut)
            signButtonElement.addEventListener("click", firebaseUI)
        }
    })
})
