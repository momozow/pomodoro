var getDocumentsOn: any
var statchart: Chart

const drawGraph = function(barChartData: object)
{
    statchart.data = barChartData
    statchart.update()
}

function getDocumentsAndDrawGraph(date: number)
{
    getDocumentsOn(date).then(result =>
    {
        console.log("show got documents")
        console.log(result)

        const startTimeArray = result.docs.map(doc =>
        {
            const milliseconds = (doc.data().start.seconds * 1000) + (doc.data().start.nanoseconds / 1000)
            return new Date(milliseconds)
        })

        const stintMinutes = result.docs.map(doc => (doc.data().finish.seconds - doc.data().start.seconds) / 60)
        const breakMinutes = result.docs.map(doc => 5)

        const barChartData = {
	    datasets: [
                                          {
	                                      backgroundColor: "#F3B9DB",
	                                      data: stintMinutes,
	                                  },
                                          {
	                                      backgroundColor: "#B5FFAA",
                                              data: breakMinutes,
	                                  },
                                      ],
                                      labels: startTimeArray,
                                  }

                                  drawGraph(barChartData)
                              })
        .catch(error =>
               {
                   console.error(error)
               })
}

document.addEventListener('DOMContentLoaded',
    () =>
    {
        const chartConfig: Object =
        {
            options:
            {
                scales:
                {
                    xAxes: [
                        {
                            stacked: true,
                            time:
                            {
                                unit: 'hour',
                            },
                            type: 'time',
                        }
                    ],
                    yAxes: [
                        {
                            stacked: true,
                        },
                    ],
                },
            },
            type: 'bar',
        }

        const myChartElement = <HTMLCanvasElement>document.getElementById('myChart')
        const context = myChartElement.getContext('2d')

        statchart = new Chart(context, chartConfig)

        const datePickerElement = <HTMLInputElement>document.getElementById('datePicker')
        datePickerElement.addEventListener('change',
            (e) =>
            {
                const pickerElement = <HTMLInputElement>e.srcElement
                const pickedDate: number = pickerElement.valueAsNumber
                getDocumentsAndDrawGraph(pickedDate)
            })

        function convertToHTMLInputValue(date: Date)
        {
            date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
            return date.toJSON().slice(0,10);
        }

        datePickerElement.value = convertToHTMLInputValue(new Date)

        getDocumentsAndDrawGraph(datePickerElement.valueAsNumber)

        window.addEventListener("orientationchange",
            () =>
            {
                let canvas = document.getElementById("allContainer")
                if (screen.orientation.type == "portrait-primary")
                {
                    canvas.style.height = "30vh"
                    canvas.style.width = "100vw"
                }
                else
                {
                    canvas.style.height = "100vh"
                    canvas.style.width = "100vw"
                }
            }, true)
    })
