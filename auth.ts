declare let firebase: any
declare let firebaseui: any

document.addEventListener("DOMContentLoaded", () => {
    const uiConfig = {
        callbacks: {
            signInSuccessWithAuthResult: () => {return true}
        },
        signInFlow: "redirect",
        signInOptions: [
            firebase.auth.EmailAuthProvider.PROVIDER_ID,
        ],
        signInSuccessUrl: "https://192.168.33.10:8000",
    }

    const ui = new firebaseui.auth.AuthUI(firebase.auth())
    ui.start("#firebaseui-auth-container", uiConfig)
})
